#!/bin/bash
# -*- ENCODING: UTF8 -*-

echo -e "Base de datos BD_Desarrollo"
echo -e "Borrando la base de datos antigua."
dropdb -i --if-exists -U IS2 BD_Desarrollo
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos, verifique que nadie la este usando."
    exit 1
fi
echo -e "Se ha borrado BD_Desarrollo."
echo -e "Creando nueva BD_Desarrollo."
createdb -U IS2 BD_Desarrollo
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear BD_Desarrollo"
    exit 2
fi
echo -e "Se ha creado BD_Desarrollo."

#echo -n "Para Restaurar los DATOS desde el archivo populate_script.py, presione [ENTER]: "
#read name

python3 manage.py makemigrations
python3 manage.py migrate
python3 populate_script.py

echo -e "BD_Desarrollo se cargo exitosamente."

