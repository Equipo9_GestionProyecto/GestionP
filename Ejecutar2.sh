#!/bin/bash
# -*- ENCODING: UTF8 -*-

echo -e "Base de datos BD_Produccion"
echo -e "Borrando la base de datos antigua."
dropdb -i --if-exists -U IS2 BD_Produccion
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo borrar la base de datos, verifique que nadie la este usando."
    exit 1
fi
echo -e "Se ha borrado BD_Produccion."
echo -e "Creando nueva BD_Produccion."
createdb -U IS2 BD_Produccion
if [ "$?" -ne 0 ]
then
    echo -e "No se pudo crear BD_Desarrollo"
    exit 2
fi
echo -e "Se ha creado BD_Produccion."

#echo -n "Para Restaurar los DATOS desde el archivo populate_script2.py, presione [ENTER]: "
#read name

python3 manage.py makemigrations --database=produccion
python3 manage.py migrate --database=produccion
python3 populate_script2.py

echo -e "BD_Produccion se cargo exitosamente."