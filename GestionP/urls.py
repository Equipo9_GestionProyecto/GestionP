"""GestionP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url, patterns
from django.contrib import admin
from django import setup
setup()
admin.autodiscover()

urlpatterns =  [
    url(r'^$', include('RegistroApp.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^usuarios/', include('usuarios.urls')),
    #url(r'^cliente/', include('cliente.urls')),
    url(r'^roles/',include('roles.urls')),
    url(r'^proyectos/',include('proyectos.urls')),
    url(r'^equipos/', include('equipos.urls')),
    url(r'^flujos/', include('flujos.urls')),
    url(r'^actividades/', include('actividades.urls')),
    url(r'^userStories/', include('userStories.urls')),
    url(r'^sprints/', include('sprints.urls')),
    url(r'^trabajos/', include('trabajos.urls')),
]