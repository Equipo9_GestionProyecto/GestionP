from django import forms
from django.contrib.auth.forms import UserCreationForm

class UserForm(UserCreationForm):

    first_name = forms.CharField(label=("Nombre"))
    last_name = forms.CharField(label=("Apellido"))
    email = forms.EmailField(label=("Correo electronico"))
    telefono = forms.IntegerField(label=("Telefono"))
    direccion = forms.CharField(label=("Direccion"))

#_______________________________________________________________________________________________________________________