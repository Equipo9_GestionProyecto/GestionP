from django.db import models
from django.contrib.auth.models import User



class Perfiles(models.Model):

    usuario = models.OneToOneField(User)
    telefono = models.IntegerField(default=0,blank=True)
    '''
       telefono es numero telefonico perteneciente al usuario
    '''
    direccion = models.CharField(max_length=64,default='tu direccion',blank=True)
    '''
       direccion es direccion en la cual reside el usuario
    '''
    lider = models.BooleanField(null=False, default=0)
    '''
       representa si el usuario en cuestion es Scrum Master o no
    '''

    def __unicode__(self):
        return self.usuario.username