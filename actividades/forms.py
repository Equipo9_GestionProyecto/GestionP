from django.forms import ModelForm
from .models import Actividad

class ActividadForm(ModelForm):
    class Meta:
        model = Actividad
        exclude = ()

class CrearActividadForm(ModelForm):

    class Meta:
        model = Actividad
        fields = ('nombre',)

class ModificarActividadForm(ModelForm):
    class Meta:
        model = Actividad
        fields = ('nombre','orden',)
