from django.conf.urls import url
from django.contrib import admin
admin.autodiscover()
from .views import listar_actividades, registrar_actividad, detalle_actividad, buscar_actividades
from .views import editar_actividad, eliminar_actividad

urlpatterns = [

        url(r'^registrar/(?P<id_flujo>\d+)$',registrar_actividad, name='registrar_actividades'),
        url(r'^flujo/(?P<id_flujo>\d+)$', listar_actividades, name='list_actividad'),
        url(r'^editar/(?P<id_actividad>\d+)$', editar_actividad, name='edit_actividad'),
        url(r'^(?P<id_actividad>\d+)$', detalle_actividad, name='detalle_actividad'),
        url(r'^eliminar/(?P<id_actividad>\d+)$', eliminar_actividad, name='eliminar_actividad'),
        url(r'^search/(?P<id_flujo>\d+)$',buscar_actividades, name='buscar_actividades'),
]