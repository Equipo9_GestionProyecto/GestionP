from django.db import models
from django.contrib.auth.models import User, Group
from proyectos.models import Proyecto

# Create your models here.

class MiembroEquipo(models.Model):
    """
    Miembros del equipo de un proyecto con un rol especifico
    usuario: Clave foranea a un Usuario
    proyecto: Clave foranea a un Proyecto
    roles: Clave muchos a muchos a Roles
    """
    usuario = models.ForeignKey(User)
    proyecto = models.ForeignKey(Proyecto, null = True)
    rol = models.ManyToManyField(Group)
    horasPorDia = models.PositiveIntegerField(default=0, verbose_name= "Horas por dia")

    def __str__(self):
        return self.proyecto.nombre
