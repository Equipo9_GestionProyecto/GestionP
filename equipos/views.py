from django.shortcuts import render
from django.shortcuts import render, render_to_response, get_object_or_404,redirect
from django.contrib.auth.models import User, Group, Permission
from RegistroApp.models import Perfiles
from RegistroApp.forms import UserForm
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.contrib.auth.decorators import login_required, permission_required
from proyectos.models import Proyecto
from flujos.models import Flujo
from .models import MiembroEquipo
from .forms import crearEquipoForm
from django.db.models import Q

# Create your views here.
@login_required
#@permission_required('proyectos')
def ver_equipo(request, id_proyecto):
    """
    vista para ver todos los usuarios que forman parte de un proyectos
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/ver_equipo.html', {'proyectos':dato,'lider': lider, 'comite':comite, 'usuarios':usuarios}, context_instance=RequestContext(request))
    """
    rolSM = Group.objects.filter(name="Scrum Master")
    permission_required(perm=rolSM)
    fuerzaTrabajo = 0
    cantHorasUS = 0
    #rolSM = Group.objects.filter(name = "Scrum Master")
    haySM = MiembroEquipo.objects.filter(rol = rolSM, proyecto_id = id_proyecto)
    if haySM.count() > 0: #si hay Scrum Master
        SM = MiembroEquipo.objects.get(rol = rolSM, proyecto_id = id_proyecto)
        SMUser = User.objects.get(id = SM.usuario_id)
        cantHorasUS = SM.horasPorDia
        fuerzaTrabajo = cantHorasUS
    else:
        SMUser = None
    proyecto = get_object_or_404(Proyecto, pk=id_proyecto)
    equipos = MiembroEquipo.objects.filter(~Q(rol = rolSM), proyecto_id = id_proyecto, )
    for equipo in equipos:
        fuerzaTrabajo = fuerzaTrabajo + equipo.horasPorDia

    return render_to_response('equipos/ver_equipo.html',
                          {'proyecto': proyecto, 'equipos': equipos, 'scrumMaster': SMUser, 'cantHorasUS': cantHorasUS, 'fuerzaTrabajo': fuerzaTrabajo},
                          context_instance=RequestContext(request))


@login_required
#@permission_required('Group.objects.filter(name="Scrum Master")')
def agregar_miembro(request, id_proyecto):
     """
    vista para agregar usuarios a un equipo de trabajo
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/ver_equipo.html', {'proyectos':dato, 'usuarios':usuarios}, context_instance=RequestContext(request))
    """
     rolSM = Group.objects.filter(name="Scrum Master")
     permission_required(perm=rolSM)
     if request.method=='POST':
        #formset = ItemFormSet(request.POST)
        formulario = crearEquipoForm(request.POST)

        if formulario.is_valid():
            formulario.save()
            equipoNuevo = MiembroEquipo.objects.filter(proyecto_id = None)
            for equipo in equipoNuevo:
                miembroEquipo = MiembroEquipo.objects.get(id = equipo.id)
                miembroEquipo.proyecto_id = id_proyecto
                miembroEquipo.save()
            return render_to_response('equipos/creacion_correcta.html',{'id_proyecto': id_proyecto}, context_instance=RequestContext(request))
     else:
        proyecto = Proyecto.objects.get(pk = id_proyecto)
        formulario = crearEquipoForm()
        return render_to_response('equipos/agregar_miembro.html', { 'formulario': formulario, 'id_proyecto': id_proyecto},
                                  context_instance=RequestContext(request))



@login_required
#@permission_required('proyectos')
def eliminar_miembro(request,id_miembro): #id_miembro = id_MiembroEquipo
    """
    Vista para eliminar un miembro de un equipo. Busca la sprint por su id_sprint y lo destruye.
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_sprint: referencia a la flujo dentro de la base de datos
    render_to_response('sprints/listar_sprints.html', {'datos': flujos, 'proyecto' : proyecto}, context_instance=RequestContext(request))
    """
    rolSM = Group.objects.filter(name="Scrum Master")
    permission_required(perm=rolSM)

    miembro = get_object_or_404(MiembroEquipo, pk=id_miembro)
    id_proyecto = miembro.proyecto_id
    miembro.delete() # borra despues de saber el id del proyecto

    fuerzaTrabajo = 0
    cantHorasUS = 0
    #rolSM = Group.objects.filter(name = "Scrum Master")
    haySM = MiembroEquipo.objects.filter(rol = rolSM, proyecto_id = id_proyecto)
    if haySM.count() > 0: #si hay Scrum Master
        SM = MiembroEquipo.objects.get(rol = rolSM, proyecto_id = id_proyecto)
        SMUser = User.objects.get(id = SM.usuario_id)
        cantHorasUS = SM.horasPorDia
        fuerzaTrabajo = cantHorasUS
    else:
        SMUser = None
    proyecto = get_object_or_404(Proyecto, pk=id_proyecto)
    equipos = MiembroEquipo.objects.filter(~Q(rol = rolSM), proyecto_id = id_proyecto)
    for equipo in equipos:
        fuerzaTrabajo = fuerzaTrabajo + equipo.horasPorDia


    return render_to_response('equipos/ver_equipo.html',
                          {'proyecto': proyecto, 'equipos': equipos, 'scrumMaster': SMUser, 'cantHorasUS': cantHorasUS, 'fuerzaTrabajo': fuerzaTrabajo},
                          context_instance=RequestContext(request))


