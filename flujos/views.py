from django.contrib.auth.decorators import login_required, permission_required
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect
from flujos.models import Flujo
from django.db.models import Q
from proyectos.models import Proyecto
from .forms import FlujoForm, ModificarFlujoForm, CrearFlujoForm
from datetime import datetime
from django.contrib.auth.models import Group
from userStories.models import UserStory
from actividades.models import Actividad

@login_required
def registrar_flujo(request, id_proyecto):
    """
    Vista para registrar un nuevo flujo dentro de proyecto
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    HttpResponseRedirect('/flujos/register/success') si el rol fue correctamente asignado o
    render_to_response('proyectos/registrar_proyecto.html',{'formulario':formulario}, context_instance=RequestContext(request)) al formulario
    """
    mensaje=100
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if request.method=='POST':
        proyecto = Proyecto.objects.get(id=id_proyecto)
        formulario = CrearFlujoForm(request.POST)
        if formulario.is_valid():
            newFlujo = Flujo(nombre = request.POST["nombre"],descripcion = request.POST["descripcion"],
                             estado = "PRO", proyecto_id = id_proyecto)
            orden=Flujo.objects.filter(proyecto_id=id_proyecto)
            proyecto=Proyecto.objects.get(id=id_proyecto)
            cantidad = orden.count()
            if cantidad>0:
                newFlujo.orden=orden.count()+1 # Calculo del orden del flujo a crear
                newFlujo.save()
                return render_to_response('flujos/creacion_correcta.html',{'id_proyecto':id_proyecto}, context_instance=RequestContext(request))
            else:
                newFlujo.orden=1
                newFlujo.save()
                return render_to_response('flujos/creacion_correcta.html',{'id_proyecto':id_proyecto}, context_instance=RequestContext(request))
    else:
        formulario = CrearFlujoForm() #formulario inicial
    return render_to_response('flujos/registrar_flujos.html',{'formulario':formulario,'id':id_proyecto, 'proyecto':proyecto, 'mensaje':mensaje},
                              context_instance=RequestContext(request))



@login_required
def listar_flujos(request,id_proyecto):
    """
    vista para listar los flujos del proyectos
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return render_to_response('flujos/listar_flujos.html', {'datos': flujos}, context_instance=RequestContext(request))
    """
    mensaje = 0 # sin errores
    flujos = Flujo.objects.filter(proyecto_id=id_proyecto).order_by('orden')
    proyecto = Proyecto.objects.get(id=id_proyecto)
    return render_to_response('flujos/listar_flujos.html', {'datos': flujos, 'proyecto' : proyecto, 'mensaje': mensaje}, context_instance=RequestContext(request))


@login_required
def editar_flujo(request,id_flujo):
    """
        Vista para editar un flujo
        @param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
        @param id_proyecto: referencia al proyecto de la base de datos
        @return: HttpResponseRedirect('/proyectos/register/success/') cuando el formulario es validado correctamente o render_to_response('proyectos/editar_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
        """
    flujo = Flujo.objects.get(id=id_flujo)
    id_proyecto = flujo.proyecto_id
    proyecto = Proyecto.objects.get(id=id_proyecto)

    if request.method == 'POST':
        # formulario enviado
        mensaje = 100
        flujo_form = ModificarFlujoForm(request.POST, instance=flujo)
        if flujo_form.is_valid():
            flujo_form.save()
            return render_to_response('flujos/creacion_correcta.html', {'id_proyecto': id_proyecto},
                                      context_instance=RequestContext(request))
    else:
        # formulario inicial
        flujo_form = ModificarFlujoForm(instance=flujo)
    return render_to_response('flujos/editar_flujo.html', {'form': flujo_form, 'flujo': flujo, 'proyecto': proyecto},
                              context_instance=RequestContext(request))


@login_required
def flujos_todas(request,id_proyecto):
    """
    vista para listar las flujos del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('flujos/flujos_todas.html', {'datos': flujos, 'proyecto' : proyecto}, context_instance=RequestContext(request))
    """
    flujos = Flujo.objects.all()
    proyecto = Proyecto.objects.get(id=id_proyecto)
    return render_to_response('flujos/flujos_todas.html', {'datos': flujos, 'proyecto' : proyecto}, context_instance=RequestContext(request))


@login_required
def detalle_flujo(request, id_flujo):

    """
    Vista para ver los detalles del flujo del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response
    """
    dato = get_object_or_404(Flujo, pk=id_flujo)
    proyecto = Proyecto.objects.get(id=dato.proyecto_id)

    return render_to_response('flujos/detalle_flujo.html', {'datos': dato,'proyecto':proyecto}, context_instance=RequestContext(request))

@login_required
def eliminar_flujo(request,id_flujo):
    """
    Vista para eliminar un flujo de un proyecto. Busca la flujo por su id_flujo y lo destruye.
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response('flujos/listar_flujos.html', {'datos': flujos, 'proyecto' : proyecto}, context_instance=RequestContext(request))
    """

    mensaje = 0 # sin errores
    flujo = get_object_or_404(Flujo, pk=id_flujo)
    proyecto = Proyecto.objects.get(id=flujo.proyecto_id)
    #if proyecto.estado =='PRO':
    flujo.delete()
    flujos = Flujo.objects.filter(proyecto_id=id_flujo).order_by('orden')

    return render_to_response('flujos/listar_flujos.html', {'datos': flujos, 'proyecto' : proyecto, 'mensaje': mensaje}, context_instance=RequestContext(request))


@login_required
def buscar_flujos(request,id_proyecto):
    """
    vista para buscar los flujos del proyecto
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return: render_to_response('proyectos/listar_proyectos.html', {'datos': results}, context_instance=RequestContext(request))
    """
    query = request.GET.get('q', '')
    proyecto = Proyecto.objects.get(id=id_proyecto)
    if query:
        qset = (
            Q(nombre__contains=query)
        )
        results = Flujo.objects.filter(qset, proyecto_id=id_proyecto).distinct()
    else:
        results = []


    return render_to_response('flujos/listar_flujos.html', {'datos': results, 'proyecto' : proyecto}, context_instance=RequestContext(request))

@login_required
def asignar_userStory(request,id_flujo):
    """
    Vista auxiliar para obtener un listado de usuarios para asociar a el flujo
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response
    """
    mensaje = 0 # sin errores
    flujo=Flujo.objects.get(id=id_flujo)
    proyecto = Proyecto.objects.get(id=flujo.proyecto_id)
    actividad1 = Actividad.objects.filter(flujo_id = id_flujo, orden = 1)
    cantActividad = actividad1.count()
    if cantActividad == 0:
        flujos = Flujo.objects.filter(proyecto_id=proyecto.id).order_by('orden')
        mensaje = 100 # Debe crear actividades al flujo antes de asignarle User Stories
        return render_to_response('flujos/listar_flujos.html', {'datos': flujos, 'proyecto' : proyecto, 'mensaje': mensaje}, context_instance=RequestContext(request))
    userStories=UserStory.objects.filter(proyecto_id = flujo.proyecto)

    # roles=Group.objects.filter(flujo__id=id_flujo)
    # for rol in roles:       #Un usuario tiene un rol por flujo
    #     usuarios=usuarios.exclude(groups__id=rol.id)

    return render_to_response('flujos/asignar_userStories.html', {'userStories': userStories, 'flujo' : flujo,'proyecto':proyecto}, context_instance=RequestContext(request))

@login_required
def asignar_userStoryFlujo(request, id_userStory, id_flujo):
    """
    Vista auxiliar para obtener un listado de user stories para asociar a el flujo
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response
    """

    flujo=Flujo.objects.get(id=id_flujo)
    userStory=UserStory.objects.get(id=id_userStory)
    userStory.estadoScrum = 1
    userStory.flujo_id = id_flujo
    actividad1 = Actividad.objects.get(flujo_id = id_flujo, orden = 1)
    userStory.actividad_id = actividad1.id
    userStory.save()
    mensaje = 100

    proyecto = Proyecto.objects.get(id=flujo.proyecto_id)
    userStories=UserStory.objects.filter(proyecto_id = flujo.proyecto)

    # roles=Group.objects.filter(flujo__id=id_flujo)
    # for rol in roles:       #Un usuario tiene un rol por flujo
    #     usuarios=usuarios.exclude(groups__id=rol.id)

    return render_to_response('flujos/asignar_userStories.html', {'userStories': userStories, 'flujo' : flujo,'proyecto':proyecto, 'mensaje' : mensaje}, context_instance=RequestContext(request))

@login_required
def desasignar_userStoryFlujo(request, id_userStory, id_flujo):
    """
    Vista auxiliar para obtener un listado de user stories para desasociar a el flujo
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response
    """

    flujo=Flujo.objects.get(id=id_flujo)
    userStory=UserStory.objects.get(id=id_userStory)
    userStory.estadoScrum = 0
    userStory.flujo_id = None
    userStory.actividad_id = None
    userStory.save()
    mensaje = 101

    proyecto = Proyecto.objects.get(id=flujo.proyecto_id)
    userStories=UserStory.objects.filter(proyecto_id = flujo.proyecto)

    # roles=Group.objects.filter(flujo__id=id_flujo)
    # for rol in roles:       #Un usuario tiene un rol por flujo
    #     usuarios=usuarios.exclude(groups__id=rol.id)

    return render_to_response('flujos/asignar_userStories.html', {'userStories': userStories, 'flujo' : flujo,'proyecto':proyecto, 'mensaje' : mensaje}, context_instance=RequestContext(request))


@login_required
def desasignar_usuario(request,id_flujo):
    """
    vista para listar a los usuario de un flujo, para poder desasociarlos
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_flujo: referencia a la flujo dentro de la base de datos
    return: render_to_response('flujos/desasignar_usuarios.html', {'datos': usuarios,'flujo':id_flujo,'proyecto':proyecto}, context_instance=RequestContext(request))
    """
    flujo=Flujo.objects.get(id=id_flujo)
    proyecto = Proyecto.objects.get(id=flujo.proyecto_id)
    if proyecto.estado!='PRO':
        proyectos = Proyecto.objects.all().exclude(estado='ELI')
        return render_to_response('proyectos/listar_proyectos.html', {'datos': proyectos,'mensaje':1},
                                  context_instance=RequestContext(request))
    roles=Group.objects.filter(flujo__id=id_flujo)
    usuarios=[]
    for rol in roles:
        p=User.objects.filter(groups__id=rol.id)
        for pp in p:
            usuarios.append(pp) #lista todos los usuarios con rol en el flujo
    return render_to_response('actividades/desasignar_usuarios.html', {'datos': usuarios,'flujo':flujo,'proyecto':proyecto,'roles':roles}, context_instance=RequestContext(request))

@login_required
def moverUS_siguiente(request, id_userStory):
    """
    Permite mover los user stories al siguiente estado
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/cambiar_estado_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """
    userStory = UserStory.objects.get(id=id_userStory)
    flujo = Flujo.objects.get(id = userStory.flujo_id)
    actividad = Actividad.objects.get(id = userStory.actividad_id)
    actividades = Actividad.objects.filter(flujo_id=flujo.id)
    cantActividades = actividades.count()

    print(userStory.estadoKanban)
    print(actividad.orden)
    print(cantActividades)

    if userStory.estadoKanban==3: # si su estado es pendiente de aprobacion va a aprobado
        userStory.estadoKanban = 4
        userStory.save()
    else:
        if actividad.orden == cantActividades: #si es la ultima actividad
            if userStory.estadoKanban == 2: # si es done pasar a pendiente de aprobacion
                userStory.estadoKanban = 3
                userStory.save()
            else: #sino es done pasa al siguiente estado doing o done
                userStory.estadoKanban = userStory.estadoKanban + 1
                userStory.save()
        else: #no es la ultima actividad
            if userStory.estadoKanban == 2: # cambio de actividad
                actSiguiente = Actividad.objects.get(flujo_id=flujo.id, orden = actividad.orden + 1)
                print(actSiguiente)
                userStory.estadoKanban = 0
                userStory.actividad_id = actSiguiente.id
                userStory.save()
            else:
                userStory.estadoKanban = userStory.estadoKanban + 1
                userStory.save()

    #vuelve a actualizar la pagina
    userStories = UserStory.objects.filter(flujo=flujo.id)
    actividades = Actividad.objects.filter(flujo_id=flujo.id)
    cantActividades = actividades.count()
    return render_to_response('flujos/kanban.html', {'userStories': userStories, 'actividades': actividades, 'cantActividades': cantActividades}, context_instance=RequestContext(request))


@login_required
def moverUS_desaprobrar(request, id_userStory):
    """
    Desaprobar el movimiento de los user stories
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/cambiar_estado_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """
    userStory = UserStory.objects.get(id=id_userStory)
    flujo = Flujo.objects.get(id = userStory.flujo_id)
    actividades = Actividad.objects.filter(flujo_id=flujo.id)
    cantActividades = actividades.count()

    ultActividad = Actividad.objects.get(flujo_id=flujo.id, orden=cantActividades)
    userStory.estadoKanban = 1
    userStory.actividad_id = ultActividad.id
    userStory.save()

    #vuelve a actualizar la pagina
    userStories = UserStory.objects.filter(flujo=flujo.id)
    actividades = Actividad.objects.filter(flujo_id=flujo.id)
    cantActividades = actividades.count()
    return render_to_response('flujos/kanban.html', {'userStories': userStories, 'actividades': actividades, 'cantActividades': cantActividades}, context_instance=RequestContext(request))

@login_required
def moverUS_desaprobrarAct(request, id_userStory):
    """
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/cambiar_estado_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """

    userStory = UserStory.objects.get(id=id_userStory)
    flujo = Flujo.objects.get(id = userStory.flujo_id)
    userStory.estadoKanban = 1
    userStory.save()

    #vuelve a actualizar la pagina
    userStories = UserStory.objects.filter(flujo=flujo.id)
    actividades = Actividad.objects.filter(flujo_id=flujo.id)
    cantActividades = actividades.count()
    return render_to_response('flujos/kanban.html', {'userStories': userStories, 'actividades': actividades, 'cantActividades': cantActividades}, context_instance=RequestContext(request))


@login_required
def estadoKanban(request, id_flujo):
    """
    Muestra el tablero kanban con los estados
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/cambiar_estado_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """
    #formulario inicial
    #flujo=Flujo.objects.get(id=id_flujo)
    userStories = UserStory.objects.filter(flujo=id_flujo)
    actividades = Actividad.objects.filter(flujo_id=id_flujo)
    cantActividades = actividades.count()
    print("cantactividades")
    print(cantActividades)
    return render_to_response('flujos/kanban.html', {'userStories': userStories, 'actividades': actividades, 'cantActividades': cantActividades}, context_instance=RequestContext(request))



