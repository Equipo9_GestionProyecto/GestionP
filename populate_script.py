import os



def populate():
    cli = agregar_rol('Cliente')
    adm = agregar_rol('Administrador')
    des = agregar_rol('Desarrollador')
    sm = agregar_rol('Scrum Master')

#Usuarios Administradores
    a = agregar_usuario('albafranco','Alba', 'Franco')
    s = agregar_usuario('sofiarivas','Sofia', 'Rivas')
    l = agregar_usuario('leticiamartinez','Leticia', 'Martinez')
    m = agregar_usuario('mariellugo','Mariel', 'Lugo')

#Usuarios Scrum Masters
    sc1 = agregar_usuario('talavera','Juan', 'Talavera')
    sc2 = agregar_usuario('cappo','Christian', 'Cappo')
    sc3 = agregar_usuario('ihara','Diego', 'Ihara')
    sc4 = agregar_usuario('pinto','Diego', 'Pinto')

#Usuarios Clientes
    c1 = agregar_usuario('Konecta','Konecta','S.A')
    c2 = agregar_usuario('Tigo','Millicom International','S.A.')
    c3 = agregar_usuario('Personal','Telecom Personal','S.A.')
    c4 = agregar_usuario('Excelsis','Excelsis','S.A.')

#Usuarios Desarrolladores
    d1 = agregar_usuario('chucknorris','Carlos', 'Gonzalez')
    d2 = agregar_usuario('corrales','Dario', 'Corrales')
    d3 = agregar_usuario('magnolia','Magnolia', 'Moran')
    d4 = agregar_usuario('cabrera','Cesar', 'Cabrera')
    d5 = agregar_usuario('rojitas','Hector', 'Rojas')
    d6 = agregar_usuario('gonzalez','Vicente', 'Gonzalez')
    d7 = agregar_usuario('coronel','Jose', 'Coronal')
    d8 = agregar_usuario('franco','Antonio De La Paz', 'Franco')
    d9 = agregar_usuario('zalazar','Jose', 'Zalazar')
    d10 = agregar_usuario('martinez','Edgar', 'Martinez')
    d11 = agregar_usuario('lima','Joaquin', 'Lima')
    d12 = agregar_usuario('arrieta','Hernan', 'Arrieta')

#Super Usuario
    gp = agregar_superUsuario('gestionp')

#Proyectos
    p1 = agregar_proyecto('Proyecto Konecta','Tigo Konecta','Desarrollo .NET para Tigo Konecta')
    p2 = agregar_proyecto('Proyecto Tigo','Tigo en Tu Comunidad', 'Desarrollo Android para Tigo en Tu Comunidad')
    p3 = agregar_proyecto('Proyecto Personal','Proyecto Telecom', 'Proyecto de Desarrollo Ruby para Personal')
    p4 = agregar_proyecto('Proyecto ProCar','ProCar', 'Nuevo Sistema para el Registro Automotor')

#Equipo 1
    agregar_miembrosEquipo(a, p1, adm)
    agregar_miembrosEquipo(c1, p1, cli)
    agregar_miembrosEquipo(sc1, p1, sm)
    agregar_miembrosEquipo(d1, p1, des)
    agregar_miembrosEquipo(d2, p1, des)
    agregar_miembrosEquipo(d3, p1, des)

#Equipo 2
    agregar_miembrosEquipo(s, p2, adm)
    agregar_miembrosEquipo(c2, p2, cli)
    agregar_miembrosEquipo(sc2, p2, sm)
    agregar_miembrosEquipo(d4, p2, des)
    agregar_miembrosEquipo(d5, p2, des)
    agregar_miembrosEquipo(d6, p2, des)

#Equipo 3
    agregar_miembrosEquipo(l, p3, adm)
    agregar_miembrosEquipo(c3, p3, cli)
    agregar_miembrosEquipo(sc3, p3, sm)
    agregar_miembrosEquipo(d7, p3, des)
    agregar_miembrosEquipo(d8, p3, des)
    agregar_miembrosEquipo(d9, p3, des)
#Equipo 4
    agregar_miembrosEquipo(m, p4, adm)
    agregar_miembrosEquipo(c4, p4, cli)
    agregar_miembrosEquipo(sc4, p4, sm)
    agregar_miembrosEquipo(d10, p4, des)
    agregar_miembrosEquipo(d11, p4, des)
    agregar_miembrosEquipo(d12, p4, des)

print ("terminado")

# TODO anadir permisos
def agregar_rol(nombre, permisos=None):
    try:
        r = Group.objects.get(name=nombre)
    except Group.DoesNotExist:
        r = Group.objects.create(name=nombre)
        r.permissions.add(1)
    return r


def agregar_usuario(user, nombre, apellido, password='gestionp'):
    try:
        u = User.objects.get(username=user)
    except User.DoesNotExist:
        u = User.objects.create_user(user, "{}@gmail.com".format(user.lower()), password)
        u.first_name=nombre
        u.last_name=apellido
        u.save()
    return u


def agregar_superUsuario(user, password='gestionp'):
    try:
        u = User.objects.get(username=user)
    except User.DoesNotExist:
        u = User.objects.create_superuser(user, "{}@gmail.com".format(user.lower()), password)
        u.is_staff = True
        u.save()
    return u


def agregar_proyecto(nombreCorto, nombre, descripcion, sprint=30):
    try:
        p = Proyecto.objects.get(nombreCorto=nombreCorto)
    except Proyecto.DoesNotExist:
        p = Proyecto.objects.get_or_create(nombreCorto=nombreCorto, nombre=nombre, descripcion=descripcion,
                                           fecha_ini=timezone.now(), fecha_fin=timezone.now() + timedelta(days=30))
    return p


def agregar_miembrosEquipo(user, project, role):

    t = MiembroEquipo.objects.create(usuario=user, proyecto=project, horasPorDia=10)
    t.rol.add(Group.objects.get(name = role.name))
    return t

# AQUI empieza la ejecucion
if __name__ == '__main__':
    print ("Inicio de la poblacion de la base de datos")
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'GestionP.settings')
    import django

    django.setup()
    from proyectos.models import Proyecto
    from django.contrib.auth.models import User, Group
    from django.utils import timezone
    from datetime import timedelta
    from equipos.models import MiembroEquipo

    populate()

