from django.db import models
from django.contrib.auth.models import User, Group

# Create your models here.
ESTADOS = (

    ('NUE', 'Nuevo'), # cuando se crea
    ('PRO', 'Produccion'), # cuando se inicia
    ('FIN','Finalizado'), # cuando se termina
    ('APR', 'Aprobado'), # cuando se aprueba uno finalizado
    ('ELI','Eliminado'), # cuando se elimina o cancela
)

class Proyecto(models.Model):
    """
    Clase del Modelo que representa al proyecto con sus atributos.
    nombre: Cadena de caracteres
    siglas: siglas del nombre del proyecto
    descripcion: Un campo de texto
    fecha_ini: Fecha que indica el inicio de un proyecto
    fecha_fin: Fecha que indica el fin estimado de un proyecto
    estado: Enum de los tipos de estados por los que puede pasar un proyecto: Pendiente, Anulado, Activo y Finalizado
    cliente: Clave foranea a la tabla usuario
    """
    nombre = models.CharField(max_length=50, verbose_name='Nombre', unique=True)
    nombreCorto = models.CharField(max_length=20, verbose_name='Nombre corto', unique=True)
    descripcion = models.TextField(verbose_name='Descripcion')
    fecha_ini = models.DateField(verbose_name='Fecha de inicio', null=True)
    fecha_fin = models.DateField(verbose_name='Fecha de Finalizacion', null=True)
    fecha_apr = models.DateField(verbose_name='Fecha de Aprobacion', null=True)
    fecha_eli = models.DateField(verbose_name='Fecha de Eliminacion', null=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    estado = models.CharField(max_length=3, choices=ESTADOS, default='NUE')
    #cliente = models.ForeignKey(Cliente, related_name='cliente')
    observacion = models.TextField(verbose_name='Observacion')

    def __unicode__(self):
        return self.id

    #class Meta:
    #    verbose_name = 'proyecto'
    #    verbose_name_plural = 'proyectos'