from django.test import TestCase, Client
from django.contrib.auth.models import User, Group
from proyectos.models import Proyecto
from django.shortcuts import resolve_url, render, redirect, get_object_or_404
from django.contrib.auth.models import Permission
from django.utils import timezone
import unittest
import django
django.setup()

# Create your tests here.
    #fixtures = ["proyecto.json"]
class ProyectoTest(TestCase):
    def setUp(self):
        """Tests donde crear los usuarios de prueba"""
        u = User.objects.create_superuser('superUsuario','superUsuario@email.com', 'superUsuario')
        #p = Permission.objects.get(codename='add_proyecto')
        #u.user_permissions.add(p)
        #p = Permission.objects.get(codename='change_proyecto')
        #u.user_permissions.add(p)
        #p = Permission.objects.get(codename='delete_proyecto')
        #u.user_permissions.add(p)
        #u= User.objects.create_user('fulano','superUsuario@email.com', 'superUsuario')
        #pro= Proyecto.objects.create(nombreCorto='Proyecto', nombre='Proyecto Largo', estado='PRO',fecha_ini=timezone.now(),fecha_fin=timezone.now(),fecha_creacion='2015-04-30 12:00',duracion_sprint='30', descripcion='Descripcion del proyecto')
        #pro = Proyecto.objects.create(nombreCorto='Proyecto', nombre='ProyectoLargo', estado='PRO',fecha_ini=timezone.now(), fecha_fin=timezone.now(),fecha_creacion='2016-05-30 12:00', descripcion='Descripcion del proyecto')
        Group.objects.create(name='rol')

    def test_permission_to_create_proyecto(self):
        """
            Test que prueba crear de un proyecto con un usuario con permisos
        """
        #c = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/proyectos/registrar')
        #self.assertEquals(response.status_code, 200)

    def test_permission_to_change_proyecto(self):
        """
            Test que prueba la modificación de un proyecto con un usuario con permisos
        """
        #Sc = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/projects/1/edit/')
        #self.assertEquals(response.status_code, 200)

    def test_permission_to_delete_proyecto(self):
        """
        Test que prueba la eliminacion de un proyecto con un usuario con permisos
        """
        #c = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/projects/1/delete/')
        #self.assertEquals(response.status_code, 200)

    def test_not_permission_to_create_proyecto(self):
        """
        Test que prueba la eliminacion de un proyecto con un usuario que no tiene permisos
        """
        #c = self.client
        #self.assertTrue(c.login(username='fulano', password='superUsuario'))
        #response = c.get('/projects/add/')
        #self.assertEquals(response.status_code, 403)

    def test_not_permission_to_change_proyecto(self):
        '''
        Test para ver si modifica un proyecto
        '''
        #c = self.client
        #self.assertTrue(c.login(username='fulano', password='superUsuario'))
        #response = c.get('/projects/1/edit/')
        #self.assertEquals(response.status_code, 403)

    def test_not_permission_to_delete_proyecto(self):
        '''
        Test para ver si elimina un proyecto sin tener permisos
        '''
        #c = self.client
        #self.assertTrue(c.login(username='fulano', password='superUsuario'))
        #response = c.get('/projects/1/delete/')
        #self.assertEquals(response.status_code, 403)

    def test_create_proyecto(self):
        '''
        Test para ver si crea un proyecto
        '''
        #c = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/proyectos/registrar/')
        #self.assertEquals(response.status_code, 200)
        #response = c.post('/proyectos/registrar', {'nombreCorto': 'test', 'nombre': 'test_proyecto','descripcion': 'test','fecha_ini': timezone.now(),'fecha_fin': timezone.now()})
        #self.assertRedirects(response, '/proyectos/{}/'.format(p.id))
        #self.assertRedirects(response, '/proyectos/1/')

    def test_edit_proyecto(self):
        '''
        Test para ver si editar un proyecto
        '''
        #c = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/proyectos/modificar/1/')
        #self.assertEquals(response.status_code, 200)
        #response = c.post('/proyectos/modificar/1', {'nombreCorto': 'Poyecto', 'nombre': 'Proyecto Largo', 'estado': 'PRO', 'fecha_ini': timezone.now(), 'fecha_fin': timezone.now(), 'fecha_creacion': '2015-03-10 18:00', 'descripcion': 'Prueba numero 800'}, follow=True)
        #p = Proyecto.objects.get(pk=1)
        #p.nombreCorto = 'Proyec'
        #p.save(update_fields=['nombreCorto'])
        #deberia redirigir
        #self.assertEquals(response.status_code, 200)
        #self.assertIsNotNone(Proyecto.objects.get(nombreCorto='Proyec'))

    def test_delete_proyecto(self):
        '''
        Test para ver si eliminar un proyecto
        '''
        #c = self.client
        #self.assertTrue(c.login(username='superUsuario', password='superUsuario'))
        #response = c.get('/proyectos/eliminar/1/')
        #self.assertEquals(response.status_code, 200)
        #response = c.post('/proyectos/eliminar/1/', {'Confirmar':True}, follow=True)
        #p = Proyecto.objects.get(pk=1)
        #p.delete()
        #self.assertRedirects(response, '/proyectos/')
        #ahora ya no deberia existir el registro
        #response = c.get('/proyectos/1/', follow=True)
        #self.assertEquals(response.status_code, 404)

    def test_listar_proyectos(self):
        '''
        Test para ver si lista correctamente un proyecto
        '''
        #print("\n\n--------Listando los proyectos-------")
        #c = Client()
        #c.login(username='admin', password='admin')
        #proyecto= Proyecto.objects.create(id=3, nombre='pruebaProyecto',descripcion='prueba',observaciones='prueba',fecha_ini='2012-12-01',fecha_fin='2013-12-01',lider_id=1)
        #resp = c.get('/proyectos/')
        #self.assertEqual(resp.status_code, 200)
        #print("Status 200, indica exito\n")
        #self.assertEqual([proyecto.pk for proyecto in resp.context['datos']], [1, 2, 3, 4, 5, 6, 7])


    def test_detalle_proyectos(self):
        '''
        Test para visualizar los detalles de un proyecto
        '''
        #c = Client()
        #c.login(username='admin', password='admin')
        #print("\n--------Listando los detalles de un proyecto-------")
        #Test para proyecto existente
        #resp = c.get('/proyectos/2')
        #self.assertEqual(resp.status_code, 200)
        #print("Status 200, indica exito\n")
        #self.assertEqual(resp.context['proyecto'].pk, 2)
        #self.assertEqual(resp.context['proyecto'].nombre, 'GestionP')
        #Test para proyecto inexistente
        #print("\nMostrando un proyecto inexistente\n")
        #resp = c.get('/proyectos/1000')
        #self.assertEqual(resp.status_code, 404)


    def test_modficar_proyecto(self):
        '''
        Test para ver si modifica correctamente un proyecto
        '''
        #c = Client()
        #print("\n\n--------Se intenta modificar los proyectos-------")
        #c.login(username='admin', password='admin')
        #test para verificar que si no modifica nada, no guarda
        #resp = c.post('/proyectos/modificar/1',follow=True)
        #self.assertEqual(resp.status_code, 200)
        #print("Status 200, indica exito, se redirige adecuadamente\n")

    def test_registrar(self):
        '''
        Test para ver si crea correctamente un proyecto
        '''
        #c = Client()
        #c.login(username='admin', password='admin')
        #print("-------------------Creando Proyectos-----")
        #prueba importar un proyecto y asignarle como nombre un nombre ya existente. Retorna un mensaje de nivel 20,
        #informando que ya existe un proyecto con ese nombre
        #resp = c.post('/proyectos/registrar/', {'nombre': 'mesa'})
        #print("Crea correctamente el proyecto,  llena el formulario de la url /proyectos/registrar/ mediante el cual llama al metodo 'crear_rol' que recibe el request")
        #self.assertEqual(resp.status_code, 200)
        #self.assertEqual(resp.context['messages'].level, 20)
        #registra correctamente y redirige a la pagina indicada
        #resp = c.post('/proyectos/registrar/',{'nombreCorto': 'Poyecto', 'nombre': 'Royecto Largo', 'estado': 'Inactivo', 'fecha_ini': timezone.now(), 'fecha_fin': timezone.now(), 'fecha_creacion': '2015-03-10 18:00', 'duracion_sprint': '30', 'descripcion': 'Prueba numero 800'}, follow=True)
        #self.assertEqual(resp.status_code, 200)
        #print("Status 200, indica exito en la operacion\n")
        #no registra correctamente ya que la fecha de fecha_ini es despues de la de fecha_fin
        #resp = c.post('/proyectos/registrar/',{'nombreCorto': 'Poyecto', 'nombre': 'Royecto Largo', 'estado': 'Inactivo', 'fecha_ini': timezone.now(), 'fecha_fin': timezone.now(), 'fecha_creacion': '2015-03-10 18:00', 'duracion_sprint': '30', 'descripcion': 'Prueba numero 800'})
        #self.assertEqual(resp.status_code, 200)
        #self.assertEqual(resp.context['messages'].level, 20)

    if __name__ == '__main__':
       unittest.main()