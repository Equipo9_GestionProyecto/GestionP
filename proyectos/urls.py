from django.conf.urls import patterns, url
from django.contrib import admin
admin.autodiscover()
from .views import lista_proyectos, registra_proyecto, RegisterSuccessView, editar_proyecto, buscar_proyecto, detalle_proyecto, \
        proyecto_iniciar, proyecto_finalizar, proyecto_aprobar, proyecto_eliminar, proyecto_rechazar, detalle_proyecto_cancelar, proyecto_cancelar

urlpatterns = [
        url(r'^registrar/$',registra_proyecto, name='registrar_proyectos'),
        url(r'^$',lista_proyectos, name='listar_proyectos'),
        url(r'^register/success/$',RegisterSuccessView ,name='RegisterSuccessView'),
        url(r'^modificar/(?P<id_proyecto>\d+)$', editar_proyecto, name='edit_proyecto'),
        url(r'^cancelar/(?P<id_proyecto>\d+)$', proyecto_cancelar, name='proyecto_cancelar'),
        url(r'^search/$',buscar_proyecto, name='buscar_proyectos'),
        url(r'^(?P<id_proyecto>\d+)$', detalle_proyecto, name='detalle_proyecto'),
        url(r'^detallecancelar/(?P<id_proyecto>\d+)$', detalle_proyecto_cancelar, name='detalle_proyecto_cancelar'),
#MANIPULACION DE LOS ESTADOS DE LOS PROYECTOS
        url(r'^iniciar/(?P<id_proyecto>\d+)$', proyecto_iniciar, name='proyecto_iniciar'),
        url(r'^finalizar/(?P<id_proyecto>\d+)$', proyecto_finalizar, name='proyecto_iniciar'),
        url(r'^aprobar/(?P<id_proyecto>\d+)$', proyecto_aprobar, name='proyecto_iniciar'),
        url(r'^eliminar/(?P<id_proyecto>\d+)$', proyecto_eliminar, name='proyecto_iniciar'),
        url(r'^rechazar/(?P<id_proyecto>\d+)$', proyecto_rechazar, name='proyecto_iniciar'),
]
print(urlpatterns)