from django.contrib.auth.decorators import login_required, permission_required
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404,redirect
from django.contrib.auth.models import User, Group
from proyectos.models import Proyecto
from proyectos.forms import ProyectoForm
from datetime import datetime
from django.http import HttpResponse, HttpResponseRedirect
from equipos.models import MiembroEquipo
from django.db.models import Q
from django.shortcuts import render
from equipos.forms import crearEquipoForm
from proyectos.forms import CancelarProyectoForm


@login_required
def lista_proyectos(request):

    userLogueadoID = request.user.id  #User Logueado ID
    #print "usuario ID %s" % userLogueado
    rolSM = Group.objects.filter(name = "Scrum Master")
    permission_required('proyectos')
    #permission_required(perm=rolSM)


    if request.user.is_staff:
        proyectos = Proyecto.objects.all()
    else:
        proyUsuario = MiembroEquipo.objects.filter(usuario_id=userLogueadoID)
        for y in proyUsuario:
            print ("Proyecto pertenece a usuario: %s" % y.proyecto_id)

        #proyectos = Proyecto.objects.all().exclude(id__in = [x.id for x in request.user.usergallery_set()])
        proyectos = Proyecto.objects.filter(id__in = [x.proyecto_id for x in proyUsuario])
        #proyecto = Proyecto.objects.get(id=id_proyecto)

        equipos = MiembroEquipo.objects.filter(rol = rolSM)
        #equipos = MiembroEquipo.objects.filter()

        #haySM = MiembroEquipo.objects.filter(rol = rolSM, proyecto_id = miembro.proyecto_id)

    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos, 'rolSM': rolSM},
                              context_instance=RequestContext(request))


@login_required
def registra_proyecto(request):
    """
    Vista para registrar un nuevo proyecto
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    HttpResponseRedirect('/proyectos/register/success') si el rol lider fue correctamente asignado o
    render_to_response('proyectos/registrar_proyecto.html',{'formulario':formulario}, context_instance=RequestContext(request)) al formulario
    """

    if request.method == 'POST':
        formulario = ProyectoForm(request.POST)
        if formulario.is_valid():
            nombre = formulario.cleaned_data['nombre']
            nombrecorto = formulario.cleaned_data['nombreCorto']
            descripcion = formulario.cleaned_data['descripcion']
            #cliente = formulario.cleaned_data['cliente']

            proyectoNvo = Proyecto(nombre=nombre, nombreCorto=nombrecorto, descripcion=descripcion)
            proyectoNvo.save()
            return redirect('/proyectos/')
    else:
        formulario = ProyectoForm()
    return render_to_response('proyectos/registrar_proyectos.html', {'formulario': formulario, 'mensaje': 1000}, context_instance=RequestContext(request))

@login_required
def RegisterSuccessView(request):
    """
    Vista llamada en caso de creacion correcta de un proyecto, redirige a un template de exito
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    render_to_response('proyectos/creacion_correcta.html', context_instance=RequestContext(request))
    """
    return render_to_response('proyectos/creacion_correcta.html', context_instance=RequestContext(request))

@login_required
def editar_proyecto(request, id_proyecto):
    """
    Vista para editar un proyecto
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: HttpResponseRedirect('/proyectos/register/success/') cuando el formulario es validado correctamente o render_to_response('proyectos/editar_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    nombre = proyecto.nombre
    if request.method == 'POST':
        # formulario enviado
        proyecto_form = ProyectoForm(request.POST, instance=proyecto)
        if proyecto_form.is_valid():
            # formulario validado correctamente
            proyecto_form.save()
            return redirect('/proyectos/')
            #return HttpResponseRedirect('/proyectos/register/success/')
    else:
        # formulario inicial
        proyecto_form = ProyectoForm(instance=proyecto)
    return render_to_response('proyectos/editar_proyecto.html', {'proyectos': proyecto_form, 'nombre': nombre},
                              context_instance=RequestContext(request))

@login_required
def buscar_proyecto(request):
    """
    vista para buscar los proyectos del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return: render_to_response('proyectos/listar_proyectos.html', {'datos': results}, context_instance=RequestContext(request))
    """
    query = request.GET.get('q', '')
    if query:
        qset = (
            Q(nombre__contains=query)
        )
        results = Proyecto.objects.filter(qset).distinct()
    else:
        results = []

    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': results},
                              context_instance=RequestContext(request))


@login_required
def detalle_proyecto(request, id_proyecto):
    """
    Vista para ver los detalles del proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    rolSM = Group.objects.filter(name="Scrum Master")
    #permission_required(perm=rolSM)
    permission_required('proyectos')
    dato = get_object_or_404(Proyecto, pk=id_proyecto)
    return render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato},
                              context_instance=RequestContext(request))

@login_required
def detalle_proyecto_cancelar(request, id_proyecto):
    """
    Vista para ver los detalles del proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    rolSM = Group.objects.filter(name="Scrum Master")
    #permission_required(perm=rolSM)
    permission_required('proyectos')
    dato = get_object_or_404(Proyecto, pk=id_proyecto)
    return render_to_response('proyectos/detalle_proyecto_cancelar.html', {'proyecto': dato},
                              context_instance=RequestContext(request))

# cambio de estado de proyecto
@login_required
def proyecto_iniciar(request, id_proyecto):
    """
    Vista para iniciar un proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    rolSM = Group.objects.filter(name = "Scrum Master")
    equipo = MiembroEquipo.objects.filter(rol = rolSM, proyecto_id = id_proyecto)
    if equipo.count() == 0:
        mensaje = 102 # no puede iniciar el proyecto sin asignar Scrum Master al equipo
        proyectos = Proyecto.objects.all()
        #proyecto = Proyecto.objects.get(id=id_proyecto)
        rolSM = Group.objects.filter(name = "Scrum Master")
        equipos = MiembroEquipo.objects.filter(rol = rolSM)
        return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos, 'mensaje': mensaje}, context_instance=RequestContext(request))
    else:
        proyectos = Proyecto.objects.get(id=id_proyecto)
        proyectos.estado = "PRO"
        proyectos.fecha_ini = datetime.now()
        proyectos.save()

        # listar proyectos
        proyectos = Proyecto.objects.all()
        return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos},
                              context_instance=RequestContext(request))

@login_required
def proyecto_finalizar(request, id_proyecto):
    """
    Vista para finalizar un proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    proyectos = Proyecto.objects.get(id=id_proyecto)
    proyectos.estado = "FIN"
    proyectos.fecha_fin = datetime.now()
    proyectos.save()

    # listar proyectos
    proyectos = Proyecto.objects.all()
    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos},
                          context_instance=RequestContext(request))

@login_required
def proyecto_aprobar(request, id_proyecto):
    """
    Vista para aprobar un proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    proyectos = Proyecto.objects.get(id=id_proyecto)
    proyectos.estado = "APR"
    proyectos.fecha_apr = datetime.now()
    proyectos.save()

    # listar proyectos
    proyectos = Proyecto.objects.all()
    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos},
                          context_instance=RequestContext(request))

@login_required
def proyecto_eliminar(request, id_proyecto):
    """
    Vista para eliminar un proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    proyectos = Proyecto.objects.get(id=id_proyecto)
    proyectos.estado = "ELI"
    proyectos.fecha_eli = datetime.now()
    proyectos.save()

    # listar proyectos
    proyectos = Proyecto.objects.all()
    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos},
                          context_instance=RequestContext(request))


@login_required
def proyecto_rechazar(request, id_proyecto):
    """
    Vista para rechazar un proyecto del sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: render_to_response('proyectos/detalle_proyecto.html', {'proyecto': dato}, context_instance=RequestContext(request))
    """
    proyectos = Proyecto.objects.get(id=id_proyecto)
    proyectos.estado = "PRO"
    proyectos.fecha_fin = None
    proyectos.save()

    # listar proyectos
    proyectos = Proyecto.objects.all()
    return render_to_response('proyectos/listar_proyectos.html', {'proyectos': proyectos},
                          context_instance=RequestContext(request))
@login_required
def proyecto_cancelar(request, id_proyecto):
    """
    Vista para cancelar un proyecto
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_proyecto: referencia al proyecto de la base de datos
    return: HttpResponseRedirect('/proyectos/register/success/') cuando el formulario es validado correctamente o render_to_response('proyectos/editar_proyecto.html', { 'proyectos': proyecto_form, 'nombre':nombre}, context_instance=RequestContext(request))
    """
    proyecto = Proyecto.objects.get(id=id_proyecto)
    nombre = proyecto.nombre
    if request.method == 'POST':
        # formulario enviado
        proyecto_form = CancelarProyectoForm(request.POST, instance=proyecto)
        if proyecto_form.is_valid():
            # formulario validado correctamente
            proyectos = Proyecto.objects.get(id=id_proyecto)
            proyectos.estado = "ELI"
            proyectos.fecha_eli = datetime.now()
            proyectos.save()
            return redirect('/proyectos/')
            #return HttpResponseRedirect('/proyectos/register/success/')
    else:
        # formulario inicial
        proyecto_form = CancelarProyectoForm(instance=proyecto)
    return render_to_response('proyectos/proyecto_cancelar.html', {'proyectos': proyecto_form, 'nombre': nombre},
                              context_instance=RequestContext(request))

