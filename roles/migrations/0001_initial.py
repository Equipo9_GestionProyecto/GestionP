# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-29 20:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('proyectos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Rol',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('nombre', models.CharField(max_length=50)),
                ('crear_usuario', models.BooleanField(default=False, verbose_name='Crear Usuario')),
                ('modificar_usuario', models.BooleanField(default=False, verbose_name='Modificar Usuario')),
                ('consultar_usuario', models.BooleanField(default=False, verbose_name='Consultar Usuario')),
                ('crear_rol', models.BooleanField(default=False, verbose_name='Crear Rol')),
                ('modificar_rol', models.BooleanField(default=False, verbose_name='Modificar Rol')),
                ('eliminar_rol', models.BooleanField(default=False, verbose_name='Eliminar Rol')),
                ('consultar_rol', models.BooleanField(default=False, verbose_name='Consultar Rol')),
                ('crear_proyecto', models.BooleanField(default=False, verbose_name='Crear Proyecto')),
                ('modificar_proyecto', models.BooleanField(default=False, verbose_name='Modificar Proyecto')),
                ('consultar_proyecto', models.BooleanField(default=False, verbose_name='Consultar Proyecto')),
                ('crear_userStory', models.BooleanField(default=False, verbose_name='Crear User Story')),
                ('modificar_userStory', models.BooleanField(default=False, verbose_name='Modificar User Story')),
                ('consultar_userStory', models.BooleanField(default=False, verbose_name='Consultar User Story')),
                ('cambiarEstado_userStory', models.BooleanField(default=False, verbose_name='Cambiar Estado User Story')),
                ('proyecto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='proyectos.Proyecto')),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
