
from django.db import models
from django.contrib.auth.models import User
from proyectos.models import Proyecto

# Create your models here.

class Rol(models.Model):
    """
    Clase del Modelo que a los roles con sus atributos.
    id : Identificador
    nombre : Cadena de caracteres
    crear_usuario: Campo booelano
    """
    id = models.AutoField(primary_key=True)
    nombre = models.CharField(max_length=50)
    usuario= models.ForeignKey(User)                             # El usuario que tiene dicho rol
    proyecto= models.ForeignKey(Proyecto, null=True)                # A que proyecto se asocia dicho rol

    #Permisos
    crear_usuario= models.BooleanField(default=False,verbose_name='Crear Usuario')
    modificar_usuario= models.BooleanField(default=False,verbose_name='Modificar Usuario')
    consultar_usuario = models.BooleanField(default=False, verbose_name='Consultar Usuario')
    crear_rol= models.BooleanField(default=False,verbose_name='Crear Rol')
    modificar_rol= models.BooleanField(default=False,verbose_name='Modificar Rol')
    eliminar_rol= models.BooleanField(default=False,verbose_name='Eliminar Rol')
    consultar_rol= models.BooleanField(default=False,verbose_name='Consultar Rol')
    crear_proyecto = models.BooleanField(default=False, verbose_name='Crear Proyecto')
    modificar_proyecto = models.BooleanField(default=False, verbose_name='Modificar Proyecto')
    consultar_proyecto = models.BooleanField(default=False, verbose_name='Consultar Proyecto')

    crear_userStory = models.BooleanField(default=False, verbose_name='Crear User Story')
    modificar_userStory = models.BooleanField(default=False, verbose_name='Modificar User Story')
    consultar_userStory = models.BooleanField(default=False, verbose_name='Consultar User Story')
    cambiarEstado_userStory = models.BooleanField(default=False, verbose_name='Cambiar Estado User Story')

    def __unicode__(self):
        return self.nombre



