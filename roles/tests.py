# -*- coding: utf-8 -*-

from django.test import Client
from django.core.management import call_command
from django.contrib.auth.models import User
from django.test import TestCase


import unittest


class RolesTestCase(TestCase):

    fixtures = ['roles']
    #call_command('loaddata', 'roles', verbosity=1)

    def test_crear_rol(self):
        '''
        Test para la creacion de un rol
        '''
        c = Client()
        c.login(username='admin', password='admin1')
        print("------------------Creando Roles-----")
        #creacion correcta del rol, redirige a la pagina correspondiente

        print ("Crea correctamente el rol Llena el formulario de la url /roles/crear/ mediante el cual llama al metodo crear_rol que recibe el request")
        resp = c.post('/roles/crear/', {'name': "Rol 1"}, follow=True)
        self.assertEqual(resp.status_code,200)
        print("Status 200, indica exito\n")
        #self.assertRedirects(resp, 'http://testserver/roles/register/success/')

        #creacion incorrecta: nombre repetido, no redirige
        #resp = c.post('/roles/crear/',{'name':"Rol 1"})
        #self.assertEqual(resp.status_code,200)

    def test_listar_roles(self):
        '''
         Test para crear un rol y ver si lo lista correctamente
        '''
        c = Client()
        print("-----Listando los roles para el usuario de prueba admin-----")
        c.login(username='admin', password='admin1')
        resp = c.get('/roles/', follow=True)
        print("Codigo 200 indica exito\n")
        self.assertEqual(resp.status_code, 200)

    def test_eliminar(self):
        '''
         Test para crear un rol y ver si lo lista correctamente
        '''
        boolu2=False
        print("--------Se muestran el exito de la operacion eliminar un rol del usuario de prueba admin-----------")
        c = Client()
        c.login(username='admin', password='admin1')
        self.test_crear_rol()
        #eliminacion de un rol existente
        resp = c.get('/roles/eliminar/4', follow=True)
        boolu2 = self.assertEqual(resp.status_code, 200)
        if boolu2:
            print ("Codigo 200 indica exito")
        #eliminacion de un rol inexistente, (ya se borro)
        #resp = c.get('/roles/eliminar/56')
        #boolu2 = self.assertEqual(resp.status_code, 404)
        #if boolu2:
        #  print("Codigo 404 indica exito, indica que no se encontro ese rol")


    def test_modificar_rol(self):
        '''
        Test para la modificacion de un rol
        '''
        c = Client()
        c.login(username='admin', password='admin1')
        bool3 = False
        print("--- Verificando validez de las url para modificar roles-----")
        #modificacion correcta del rol, redirige a la pagina correspondiente
        print("Primero se crea")
        resp = c.post('/roles/crear/',{'name':"Rol 1"})
        print("Luego se modifica")
        resp = c.post('/roles/modificar/5',{'name':"Rol 4"},follow=True)
        bool3 = self.assertEqual(resp.status_code, 200)
        if bool3:
            print("Si llego aqui redirige adecuadamente")
        #self.assertRedirects(resp, 'http://testserver/roles/register/success/')
        #modificacion incorrecta, no redirige, ya que el nombre de la fase ya existe
        #resp = c.post('/roles/crear/',{'name':"Rol 1"})
        #resp = c.post('/roles/modificar/5',{'name':"Rol 1"})
        #self.assertEqual(resp.status_code, 200)


    if __name__ == '__main__':
        unittest.main()
