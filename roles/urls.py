from django.conf.urls import url
from django.contrib import admin
admin.autodiscover()

from .views import crear_rol, detalle_rol, lista_roles, editar_rol, RegisterSuccessView, eliminar_rol

urlpatterns =  [
    url(r'^crear/$', crear_rol, name='crear_rol'),
    url(r'^$',lista_roles,name='lista_roles'),
    url(r'^(?P<id_rol>\d+)$', detalle_rol, name='detalle_rol'),
    url(r'^eliminar/(?P<id_rol>\d+)$', eliminar_rol, name='eliminar_rol'),
    url(r'^modificar/(?P<id_rol>\d+)$', editar_rol, name='editar_rol'),
    url(r'^register/success/$',RegisterSuccessView.as_view()),
]
print(urlpatterns)
