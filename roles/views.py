from django.contrib.auth.decorators import login_required, permission_required
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.models import Group, Permission
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.views.generic import TemplateView
from .forms import GroupForm
from django.shortcuts import render_to_response
from django.db.models import Q

__text__ = 'Este modulo contiene funciones que permiten el control de roles'

@login_required
def crear_rol(request):
    """
    vista para crear un rol, que consta de un nombre y una lista de permisos
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return: return HttpResponseRedirect('/roles/register/success/') o render_to_response('roles/crear_rol.html', { 'group_form': group_form}, context_instance=RequestContext(request))
    """

    if request.method == 'POST':
        # formulario enviado
        group_form = GroupForm(request.POST)

        if group_form.is_valid():
            # formulario validado correctamente
            group_form.save()
            return redirect('/roles/')
    else:
        # formulario inicial
        group_form = GroupForm()


    return render_to_response('roles/crear_rol.html', { 'group_form': group_form}, context_instance=RequestContext(request))

@login_required
def detalle_rol(request, id_rol):
    """
    vista para ver los detalles del rol <id_rol> del sistema
    @param request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    @param id_rol: referencia a los roles
    @return: render_to_response('roles/detalle_rol.html', {'rol': dato, 'permisos': permisos}, context_instance=RequestContext(request))
    """

    dato = get_object_or_404(Group, pk=id_rol)
    permisos = Permission.objects.filter(group__id=id_rol)
    return render_to_response('roles/detalle_rol.html', {'rol': dato, 'permisos': permisos}, context_instance=RequestContext(request))

@login_required
def lista_roles(request):
    """
    vista para listar los roles existentes en el sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return: render_to_response('roles/listar_roles.html', {'datos': grupos}, context_instance=RequestContext(request))
    """

    grupos = Group.objects.all()
    return render_to_response('roles/listar_roles.html', {'datos': grupos}, context_instance=RequestContext(request))

@login_required
def eliminar_rol(request, id_rol):
    """
    vista para eliminar el rol <id_rol>. Se comprueba que dicho rol no tenga usuarios asociadas.
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    id_rol: referencia a los roles
    return: render_to_response('roles/listar_roles.html', {'datos': grupos}, context_instance=RequestContext(request))
    """

    dato = get_object_or_404(Group, pk=id_rol)
    grupos = Group.objects.all()
    dato.delete()
    return render_to_response('roles/listar_roles.html', {'datos': grupos,'mensaje':1}, context_instance=RequestContext(request))


class RegisterSuccessView(TemplateView):
    template_name = 'roles/creacion_correcta.html'

@login_required
def editar_rol(request,id_rol):
    """
    vista para cambiar el nombre del rol o su lista de permisos
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    param id_rol: referencia a los roles
    return: HttpResponseRedirect('/roles/register/success/') o render_to_response('roles/editar_rol.html', { 'rol': rol_form, 'dato':rol}, context_instance=RequestContext(request))
    """
    rol= Group.objects.get(id=id_rol)
    if request.method == 'POST':
        # formulario enviado
        rol_form = GroupForm(request.POST, instance=rol)

        if rol_form.is_valid():
            # formulario validado correctamente
            rol_form.save()
            return redirect('/roles/')

    else:
        # formulario inicial
        rol_form = GroupForm(instance=rol)
    return render_to_response('roles/editar_rol.html', { 'rol': rol_form, 'dato':rol}, context_instance=RequestContext(request))

@login_required
def buscarRol(request):
    """
    vista para buscar un rol entre todos los registrados en el sistema
    request: objeto HttpRequest que representa la metadata de la solicitud HTTP
    return: return render_to_response('roles/listar_roles.html', {'datos': results}, context_instance=RequestContext(request))
    """
    query = request.GET.get('q', '')
    if query:
        qset = (
            Q(name__contains=query)
        )
        results = Group.objects.filter(qset).distinct()
    else:
        results = []
    return render_to_response('roles/listar_roles.html', {'datos': results}, context_instance=RequestContext(request))