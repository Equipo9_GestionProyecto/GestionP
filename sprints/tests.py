from django.contrib.auth.models import User, Group
from django.contrib.auth.models import Permission
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.contrib.auth.models import User, Group, Permission
from django.utils import timezone
from proyectos.models import Proyecto
from userStories.models import UserStory
from sprints.models import Sprint
from actividades.models import Actividad
import unittest


class SprintTest(TestCase):
    def setUp(self):
            """Tests para crear los usuarios de prueba"""
#            u = User.objects.create_superuser('temp','temp@email.com', 'temp')
#            pro= Proyecto.objects.create(nombre_corto='Proyecto', nombre_largo='Proyecto Largo', estado='APR',fecha_ini=timezone.now(),fecha_fin=timezone.now(),fecha_creacion=timezone.now(), descripcion='Prueba numero 800')
#            User.objects.create_user('tempdos', 'tempdos@email.com', 'tempdos')
#            UserStory.objects.create(nombre= 'Test_Version', descripcion= 'Test Description', prioridad= 1, valor_negocio= 10, valor_tecnico= 10, tiempo_estimado =10, proyecto = pro)
#            f=Flujo.objects.create(nombre ='flujo_test', proyecto= pro)
#            Actividad.objects.create(name='actividad_test', flujo=f)
#            Sprint.objects.create(nombre='sprint_test',inicio=timezone.now(),fin=timezone.now(), proyecto=pro)

    def test_to_create_sprint(self):
            """Tests para crear sprint"""
#            c = self.client
#            self.assertTrue(c.login(username='temp', password='temp'))
#            p= Proyecto.objects.first()
#            self.assertIsNotNone(p)
#            us=UserStory.objects.first()
#            self.assertIsNotNone(us)
#            u=User.objects.first()
#            self.assertIsNotNone(u)
#            f=User.objects.first()
#            self.assertIsNotNone(f)
#            response = c.get(reverse('project:sprint_add', args=(str(p.id))))
#            self.assertEquals(response.status_code, 200)
#            post_data = {
#            'nombre': 'Sprint_test',
#            'inicio': timezone.now(),
#            'proyecto':p,
#            'fin':timezone.now(),
#            'actividad': 1,
#            'tiempo_registrado': 4,
#            'estado_actividad': 1,
#            'form-INITIAL_FORMS': 0,
#            'form-MAX_NUM_FORMS': 1000,
#            'form-MIN_NUM_FORMS': 0,
#            'form-TOTAL_FORMS': 1,
#            'form-0-userStory': us.id,
#            'form-0-desarrollador':u.id,
#            'form-0-flujo':f.id,
#             }
#            response = c.post(reverse('project:sprint_add', args=(str(p.id))), post_data, follow=True)
#            self.assertEquals(response.status_code,200)
#            s=Sprint.objects.get(pk=1)
#            self.assertIsNotNone(s)
#            #self.assertRedirects(response, '/sprint/1/')

    def test_to_edit_sprint(self):
            """Tests para editar sprint de prueba"""
#            c = self.client
#            self.assertTrue(c.login(username='temp', password='temp'))
#            p= Proyecto.objects.first()
#            self.assertIsNotNone(p)
#            us=UserStory.objects.first()
#            self.assertIsNotNone(us)
#            u=User.objects.first()
#            self.assertIsNotNone(u)
#            f=Flujo.objects.create(nombre ='flujo_test2', proyecto= p)
#            Actividad.objects.create(name='actividad_test2', flujo=f)
#            self.assertIsNotNone(f)
#            s=Sprint.objects.get(pk=1)
#            response = c.get(reverse('proyectos:sprint_add', args=(str(s.id))))
#            self.assertEquals(response.status_code, 200)
#            post_data = {
#                'nombre': 'Sprint_test',
#                'inicio': timezone.now(),
#                'fin':timezone.now(),
#                'proyecto':p,
#               'form-INITIAL_FORMS': 0,
#                'form-MAX_NUM_FORMS': 1000,
#              'form-MIN_NUM_FORMS': 0,
#                'form-TOTAL_FORMS': 1,
#                'form-0-userStory': us.id,
#                'form-0-desarrollador':u.id,
#                'form-0-flujo':f.id,
#            }
#            response = c.post(reverse('proyectos:sprint_update',args=(str(s.id))), post_data, follow=True)
#            self.assertEquals(response.status_code,200)

    def test_permission_to_create_sprint(self):
        """Tests para crear sprint """
#       c = self.client
#       self.assertTrue(c.login(username='temp', password='temp'))
#       p= Proyecto.objects.first()
#       self.assertIsNotNone(p)
#       response = c.get(reverse('proyectos:sprint_add', args=(str(p.id))))
#       self.assertEquals(response.status_code, 200)

    def test_permission_to_no_create_sprint(self):
        """Tests para crear sprint de prueba sin tener permisos """
#            c = self.client
#            self.assertTrue(c.login(username='tempdos', password='tempdos'))
#            p= Proyecto.objects.first()
#            self.assertIsNotNone(p)
#            response = c.get(reverse('proyectos:sprint_add', args=(str(p.id))))
#            self.assertEquals(response.status_code, 403)

    def test_permission_to_edit_sprint(self):
        """Tests para editar sprints"""
#            c = self.client
#            self.assertTrue(c.login(username='temp', password='temp'))
#            p= Proyecto.objects.first()
#            self.assertIsNotNone(p)
#            s=Sprint.objects.first()
#            response = c.get(reverse('proyectos:sprint_update', args=(str(s.id))))
#            self.assertEquals(response.status_code, 200)

    def test_permission_to_no_edit_sprint(self):
        """Tests para editar sprints sin tener permisos"""
#            c = self.client
#            self.assertTrue(c.login(username='tempdos', password='tempdos'))
#            p= Proyecto.objects.first()
#            self.assertIsNotNone(p)
#            s=Sprint.objects.first()
#            response = c.get(reverse('proyectos:sprint_update', args=(str(s.id))))
#            self.assertEquals(response.status_code, 403)

    if __name__ == '__main__':
            unittest.main()