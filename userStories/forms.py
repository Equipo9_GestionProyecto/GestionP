from django import forms
from userStories.models import UserStory

ESTADOS = (

    ('PEN', 'Pendiente'),
    #('FIN','Finalizado'),
    ('VAL', 'Validado'),
)

class crearUserStoryForm(forms.ModelForm):
    class Meta:
        model= UserStory
        exclude = ('actividad', 'flujo', 'estadoKanban', 'tiempo_registrado', 'estadoScrum', 'proyecto', 'desarrollador', 'sprint', 'version', 'orden', 'fecha_mod', 'prioridad_obtenida')

class EstadoUserStoryForm(forms.ModelForm):
    estado=forms.CharField(max_length=3,widget=forms.Select(choices= ESTADOS))
    class Meta:
        model=UserStory
        fields=['estado']
