# -*- coding: utf-8 -*-
# Generated by Django 1.9.5 on 2016-06-29 20:37
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('sprints', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('flujos', '0001_initial'),
        ('proyectos', '0001_initial'),
        ('actividades', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserStory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=20, verbose_name='Nombre')),
                ('descripcion', models.TextField(verbose_name='Descripcion')),
                ('prioridad', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9)], default=1)),
                ('valor_negocio', models.PositiveIntegerField(verbose_name='Valor de Negocio')),
                ('valor_tecnico', models.PositiveIntegerField(verbose_name='Valor Tecnico')),
                ('tiempo_estimado', models.PositiveIntegerField(verbose_name='Tiempo Estimado en Horas')),
                ('tiempo_registrado', models.PositiveIntegerField(default=0, verbose_name='Tiempo Registrado')),
                ('ultimo_cambio', models.DateTimeField(auto_now=True, verbose_name='Ultimo Cambio')),
                ('estadoKanban', models.IntegerField(choices=[(0, 'ToDo'), (1, 'Doing'), (2, 'Done'), (3, 'Pendiente Aprobacion'), (4, 'Aprobado')], default=0)),
                ('estadoScrum', models.IntegerField(choices=[(0, 'Nuevo'), (1, 'Iniciado'), (2, 'Suspendido'), (3, 'Eliminado')], default=0)),
                ('version', models.PositiveIntegerField(blank=True, null=True)),
                ('orden', models.PositiveIntegerField(blank=True, null=True)),
                ('fecha_mod', models.DateField(null=True, verbose_name='Fecha de Modificacion')),
                ('prioridad_obtenida', models.PositiveIntegerField(blank=True, null=True)),
                ('actividad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='actividades.Actividad')),
                ('desarrollador', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('flujo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='flujos.Flujo')),
                ('proyecto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='proyectos.Proyecto')),
                ('sprint', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='sprints.Sprint')),
            ],
        ),
        migrations.CreateModel(
            name='VersionUserStory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=20, verbose_name='Nombre')),
                ('descripcion', models.TextField(verbose_name='Descripcion')),
                ('prioridad', models.IntegerField(choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9)], default=1)),
                ('valor_negocio', models.PositiveIntegerField(verbose_name='Valor de Negocio')),
                ('valor_tecnico', models.PositiveIntegerField(verbose_name='Valor Tecnico')),
                ('tiempo_estimado', models.PositiveIntegerField(verbose_name='Tiempo Estimado')),
                ('ultimo_cambio', models.DateTimeField(auto_now=True, verbose_name='Ultimo Cambio')),
                ('estadoKanban', models.IntegerField(choices=[(0, 'ToDo'), (1, 'Doing'), (2, 'Done'), (3, 'Pendiente Aprobacion'), (4, 'Aprobado')], default=0)),
                ('estadoScrum', models.IntegerField(choices=[(0, 'Nuevo'), (1, 'Iniciado'), (2, 'Suspendido'), (3, 'Eliminado')], default=0)),
                ('version', models.PositiveIntegerField(blank=True, null=True)),
                ('orden', models.PositiveIntegerField(blank=True, null=True)),
                ('fecha_mod', models.DateField(verbose_name='Fecha de Modificacion')),
                ('actividad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='actividades.Actividad')),
                ('desarrollador', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('flujo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='flujos.Flujo')),
                ('id_UserStory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='userStoryVersion', to='userStories.UserStory', verbose_name='UserStory')),
                ('proyecto', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='proyectos.Proyecto')),
            ],
        ),
    ]
