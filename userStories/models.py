from django.db import models
from django.contrib.auth.models import Group
from flujos.models import Flujo
from django.contrib.auth.models import User
from proyectos.models import Proyecto
from sprints.models import Sprint
from actividades.models import Actividad
#from apps.trabajos.models import Trabajo


# Create your models here.



class UserStory(models.Model):
    """
    Modelo que representa a un User Story
    nombre: Cadena de caracteres
    descripcion: Un campo de texto
    tiempo: Entero positivo que representa la cantidad de tiempo en dias
    estado: Enum de los tipos de estados por los que puede pasar una fase: Pendiente, En Construccion, Finalizado, Validado, Revision, Anulado
    version: Entero corto con valores positivos
    orden: Entero corto que representa el orden relative de items
    relacion: clave foranea a otro item
    tipo: Enum de los tipos de relaciones entre items: Padre-Padre-Antecesor-Padre
    fecha_creacion: Tipo de dato Date
    proyecto: Clave foranea a un Proyecto
    desarrollador: Clave foranea a un Desarrollador
    fecha_mod: Clave foranea a a un Sprint
    actividad: Clave foranea a una Actividad
    """
    estados_kanban = ((0, 'ToDo'), (1, 'Doing'), (2, 'Done'), (3, 'Pendiente Aprobacion'), (4, 'Aprobado'))
    estados_scrum = ((0, 'Nuevo'), (1, 'Iniciado'), (2, 'Suspendido'), (3, 'Eliminado'))

    nombre = models.CharField(max_length=20, verbose_name='Nombre')
    descripcion = models.TextField(verbose_name='Descripcion')
    prioridad = models.IntegerField(choices=((i, i) for i in range(1, 10)), default=1)
    valor_negocio = models.PositiveIntegerField(verbose_name='Valor de Negocio')
    valor_tecnico = models.PositiveIntegerField(verbose_name='Valor Tecnico')
    tiempo_estimado = models.PositiveIntegerField(verbose_name='Tiempo Estimado en Horas')
    tiempo_registrado = models.PositiveIntegerField(verbose_name='Tiempo Registrado', default=0)
    ultimo_cambio = models.DateTimeField(auto_now=True, verbose_name='Ultimo Cambio')
    estadoKanban = models.IntegerField(choices=estados_kanban, default=0)
    estadoScrum = models.IntegerField(choices=estados_scrum, default=0)
    proyecto = models.ForeignKey(Proyecto)
    desarrollador = models.ForeignKey(User, null=True)
    sprint = models.ForeignKey(Sprint, null= True)
    flujo = models.ForeignKey(Flujo, null=True, blank=True)
    actividad = models.ForeignKey(Actividad, null=True, blank=True)
    version = models.PositiveIntegerField(null=True, blank=True)
    orden = models.PositiveIntegerField(null=True, blank=True)
    fecha_mod = models.DateField(verbose_name='Fecha de Modificacion', null=True)
    prioridad_obtenida = models.PositiveIntegerField(null=True, blank=True)
    #trabajo = models.ForeignKey(Trabajo, null=True, blank=True)


    def __str__(self):
        return self.nombre




class VersionUserStory(models.Model):
    """
    Modelo que representa a la version de un  User Story
    nombre: Cadena de caracteres
    descripcion: Un campo de texto
    tiempo: Entero positivo que representa la cantidad de tiempo en dias
    estado: Enum de los tipos de estados por los que puede pasar una fase: Pendiente, En Construccion, Finalizado, Validado, Revision, Anulado
    version: Entero corto con valores positivos
    orden: Entero corto que representa el orden relative de items
    relacion: clave foranea a otro item
    tipo: Enum de los tipos de relaciones entre items: Padre-Padre-Antecesor-Padre
    fecha_creacion: Tipo de dato Date
    proyecto: Clave foranea a un Proyecto
    desarrollador: Clave foranea a un Desarrollador
    fecha_mod: Clave foranea a a un Sprint
    actividad: Clave foranea a una Actividad
    """
    estados_kanban = ((0, 'ToDo'), (1, 'Doing'), (2, 'Done'), (3, 'Pendiente Aprobacion'), (4, 'Aprobado'))
    estados_scrum = ((0, 'Nuevo'), (1, 'Iniciado'), (2, 'Suspendido'), (3, 'Eliminado'))

    id_UserStory = models.ForeignKey(UserStory,  verbose_name='UserStory', related_name='userStoryVersion')
    nombre = models.CharField(max_length=20, verbose_name='Nombre')
    descripcion = models.TextField(verbose_name='Descripcion')
    prioridad = models.IntegerField(choices=((i, i) for i in range(1, 10)), default=1)
    valor_negocio = models.PositiveIntegerField(verbose_name='Valor de Negocio')
    valor_tecnico = models.PositiveIntegerField(verbose_name='Valor Tecnico')
    tiempo_estimado = models.PositiveIntegerField(verbose_name='Tiempo Estimado')
    ultimo_cambio = models.DateTimeField(auto_now=True, verbose_name='Ultimo Cambio')
    estadoKanban = models.IntegerField(choices=estados_kanban, default=0)
    estadoScrum = models.IntegerField(choices=estados_scrum, default=0)
    proyecto = models.ForeignKey(Proyecto)
    desarrollador = models.ForeignKey(User, null=True)
    flujo = models.ForeignKey(Flujo, null=True, blank=True)
    actividad = models.ForeignKey(Actividad, null=True, blank=True)
    version = models.PositiveIntegerField(null=True, blank=True)
    orden = models.PositiveIntegerField(null=True, blank=True)
    fecha_mod = models.DateField(verbose_name='Fecha de Modificacion')
    #trabajo = models.ForeignKey(Trabajo, null=True, blank=True)

    def __str__(self):
        return self.nombre

