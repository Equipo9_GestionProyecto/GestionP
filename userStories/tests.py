
import datetime
from django.core.urlresolvers import reverse
from django.test import TestCase
from django.contrib.auth.models import User, Group, Permission
from django.utils import timezone
from flujos.models import Flujo
from proyectos.models import Proyecto
from userStories.models import UserStory
from actividades.models import Actividad
from sprints.models import Sprint

import unittest

class UserStoryTest(TestCase):

    def setUp(self):
        '''
            Test para User Story
        '''
        #u = User.objects.create_superuser('test', 'test@test.com', 'test') #Superusuario con todos los permisos
        #u2 = User.objects.create_user('none', 'none@none.com', 'none') #Usuario sin permisos
        #p = Proyecto.objects.create(nombreCorto='TestProy', nombre='TestProyectoName', estado='APR',fecha_ini=timezone.now(), fecha_fin=timezone.now(), fecha_creacion='2016-08-10 18:00', descripcion='Test')
        #p = Proyecto.objects.create(nombreCorto='TestProy', nombre='TestProyectoName', estado='', fecha_ini=timezone.now(), fecha_fin=timezone.now(), fecha_creacion='2016-06-10 18:00',descripcion='Test')

    def test_add_userstory_with_permission(self):
        '''
            Test para crear un user Story con permisos
        '''
        #c = self.client
        #login = c.login(username='test', password='test')
        #self.assertTrue(login)
        #p = Proyecto.objects.first()
        #deberia existir
        #self.assertIsNotNone(p)
        #response = c.get(reverse('/userStories/crear', args=(str(p.id))))
        #self.assertEquals(response.status_code, 200)
        #response = c.post(reverse('/userStories/crear', args=(str(p.id))),{'nombre':'Test User story', 'descripcion':'This is a User Story for testing purposes.', 'prioridad': 1,
        #'valor_negocio': 10, 'valor_tecnico': 10, 'tiempo_estimado': 10}, follow=True)
        #deberia redirigir
        #self.assertRedirects(response, '/userStories/1/')
        #us = UserStory.objects.get(pk=1)
        #self.assertIsNotNone(us)
        #response = c.get(reverse('/userStories/detalle', args=(str(us.id))))
        #self.assertEquals(response.status_code, 200)

    def test_update_userstory_with_permission(self):
        '''
           Test para eliminar un user story
        '''
        #c = self.client
        #login = c.login(username='test', password='test')
        #self.assertTrue(login)
        #p = Proyecto.objects.first()
        #creamos un user story
        #response = c.post(reverse('userStories/crear', args=(str(p.id))),
        #{'nombre':'First Value US', 'descripcion':'This is a User Story for testing purposes.', 'prioridad': 1,
        #'valor_negocio': 10, 'valor_tecnico': 10, 'tiempo_estimado': 10}, follow=True)
        #us = UserStory.objects.first()
        #self.assertIsNotNone(us)
        #self.assertEquals(us.nombre, 'First Value US')
        #response = c.get(reverse('userStories/detalle', args=(str(us.id))))
        #self.assertEquals(response.status_code, 200)
        #nos vamos a la página de edición de user story
        #response = c.get(reverse('userStories/modificar', args=(str(us.id))))
        #debería retornar 200
        #self.assertEquals(response.status_code, 200)
        #response = c.post(reverse('project:userstory_update', args=(str(p.id))),
        #{'nombre':'Second Value US', 'descripcion':'This is a User Story for testing purposes.', 'prioridad': 1,
        #'valor_negocio': 10, 'valor_tecnico': 10, 'tiempo_estimado': 10}, follow=True)
        #self.assertRedirects(response, '/userStories/1/')
        #us = UserStory.objects.first()
        #self.assertIsNotNone(us)
        #vemos que el nombre ya no es el anterior
        #self.assertNotEquals(us.nombre, 'First Value US')
        #self.assertEquals(us.nombre, 'Second Value US')

    def test_delete_userstory_with_permission(self):
       '''
        Test para ver si eliminar un user story con permisos
        '''
        #c = self.client
        #login = c.login(username='test', password='test')
        #self.assertTrue(login)
        #p = Proyecto.objects.first()
        #creamos un user story
        #response = c.post(reverse('userStories/crear', args=(str(p.id))),
        #{'nombre': 'First Value US', 'descripcion': 'This is a User Story for testing purposes.','prioridad': 1,
        #'valor_negocio': 10, 'valor_tecnico': 10, 'tiempo_estimado': 10}, follow=True)
        #us = UserStory.objects.first()
        #self.assertIsNotNone(us)
        #nos vamos a la página de borrado de user story
        #response = c.get(reverse('userStories/eliminar', args=(str(us.id))))
        #debería retornar 200
        #self.assertEquals(response.status_code, 200)
        #response = c.post(reverse('userStories/eliminar', args=(str(p.id))),{'Confirmar': True}, follow=True)
        #us = UserStory.objects.first()
        #No debería existir
        #self.assertIsNone(us)


    if __name__ == '__main__':
        unittest.main()
