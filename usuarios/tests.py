
from django.contrib.auth.models import User
from django.test import TestCase
from django.test.client import RequestFactory
from django.shortcuts import resolve_url, render, redirect, get_object_or_404
from usuarios.views import Registrarse, delete_user,edit_user
import unittest

class test_user(TestCase):
    fixtures = ['usuarios.json']
    def setUp(self):
        """Tests para crear usuarios"""
        #self.factory = RequestFactory()
    def test_usuarioEliminar(self):
        """Tests para eliminar usuarios"""
        #request = self.factory.get('/usuarios/delete/')
        #Asignamos a la variable user el usuario administrador.
        #self.user = User.objects.get(pk=1)
        #Logueamos al usuario mediante el request
        #request.user = self.user
        #Nos aseguramos que el usuario con id existe.
        #id = '2'
        #usuarioname = User.objects.get(pk=id)
        #self.assertTrue(usuarioname)
        #Borramos al usuario con id 2
        #response = delete_user(request, id)
        #Nos aseguramos que el usuario con id se ha eliminado
        #usuarioname = User.objects.get(pk=id)
        #self.assertFalse(usuarioname.is_active)
        #print('Test de eliminacion de Usuario normal ejecutado exitosamente.')

    def test_usuarioNuevo(self):
        """Tests para crear nuevos usuarios"""
        # Crea una instancia de una solicitud GET
        #self.user = User.objects.get(pk=1)

        #completamos en el request el formulario
        #request = self.factory.post('/usuarios/registrarse/', {'Usuario': 'lalala', 'Contrasenha': 'lalalapass', 'Reingresar_Contrasenha': 'lalalapass', 'Email': 'lalala@pol.una.py', 'Nombre': 'Hernan', 'Apellido': 'Medina'})
        #request.user = self.user
        #response = Registrarse(request)                #mando a la vista de creacion de nuevo usuario
        #self.assertEqual(response.status_code, 200)     #verifico que se haya recibido la pagina
        #busco = User.objects.get(username='lalala')     #busco el usuario creado
        #print('Usuario Creado: ')
        #print('\tUsuario: ' + busco.username)
        #print('\tNombre: ' + busco.first_name)
        #print('\tApellido: ' + busco.last_name)
        #print('Test de Creacion de Usuario ejecutado exitosamente.\n')

    def test_modificarUsuario(self):
        """Tests para modificar usuarios"""
        #self.user = User.objects.get(pk=1)
        #print('')
        #print(User.objects.all())
        #id = '2'                                #busco un usuario para modificar
        #request = self.factory.post('/usuarios/edit/', {'Usuario': 'MODlitatusMOD','Nombre': 'No se', 'Apellido': 'quien sabe', 'Email': 'algo@algo.com.py'})
        #request.user = self.user
        #response = edit_user(request, id)
        #self.assertEqual(response.status_code, 200)
        #print(User.objects.all())
        #print('Test de modificacion de usuario ejecutado exitosamente.')

