from django.conf.urls import  url
from .views import list_usuario, edit_user, password_change, password_change_done, search,delete_user, Registrarse

urlpatterns = [

    url(r'^$', list_usuario, name='usuario'),
    url(r'^edit/(?P<pk>\d+)/$', edit_user, name='usuario_edit'),
    url(r'^delete/(?P<pk>\d+)/$', delete_user, name='usuario_delete'),
    url(r'^nuevo_pass/$', password_change, name='cambiar_pass_done'),
    url(r'^nuevo_pass/done/$', password_change_done, name='cambiar_pass_done'),
    url(r'^search/$', search, name='search'),
    url(r'^usuarios/registrarse/$', Registrarse.as_view(), name='registrarse'),
    url(r'^cerrar/$', 'django.contrib.auth.views.logout_then_login',
        name='logout'),
]